from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
import schedule

import time
import datetime

SHORT_TIMEOUT = 10
LONG_TIMEOUT = 30

STAR_SHIP_DATE = datetime.date(2020, 12, 31)
CHAT_NAME = "MyChatName"


def send_message(driver, chat_name, message):
    # Find the search bar element.
    search_element = WebDriverWait(driver, SHORT_TIMEOUT).until(
        expected_conditions.presence_of_element_located(
            (By.CSS_SELECTOR, "#side label > input.copyable-text.selectable-text"))
    )

    # Enter the chat name.
    search_element.send_keys(f"{chat_name}")

    # Send the chat name.
    search_element.send_keys(Keys.RETURN)

    # Find the input element for this chat.
    input_element = WebDriverWait(driver, SHORT_TIMEOUT).until(
        expected_conditions.presence_of_element_located(
            (By.CSS_SELECTOR, "#main > footer div.copyable-text.selectable-text"))
    )

    # Enter the required message.
    input_element.send_keys(f"{message}")

    # Send the message.
    input_element.send_keys(Keys.RETURN)

    # Wait a small delay.
    time.sleep(3)


def get_days_until(future_date):
    diff = future_date - datetime.date.today()
    return diff.days


def create_prominent_message():
    return f"PROMINENT'S COUNTDOWN {get_days_until(STAR_SHIP_DATE)} DAYS REMAINING!"


def send_prominent_alert():
    # Log current date for debugging.
    current_date = datetime.datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
    print(f"Running on {current_date}...")

    # Create the chrome driver with the required options.
    chrome_options = webdriver.ChromeOptions()
    # chrome_options.add_argument("--headless")
    # chrome_options.add_argument("--no-sandbox")
    # chrome_options.add_argument("--disable-dev-shm-usage")
    # chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("user-data-dir=prominent")
    driver = webdriver.Chrome(options=chrome_options)

    try:
        # Load the whatsApp web page and send message to the corresponding chat.
        driver.get("https://web.whatsapp.com/")
        send_message(driver, CHAT_NAME, create_prominent_message())

    except Exception as e:
        print(e)

    finally:
        # Disable are you sure you want to leave alert.
        driver.execute_script("window.onbeforeunload = function() {};")

        # Close the browser.
        driver.quit()


if __name__ == "__main__":
    schedule.every().second.do(send_prominent_alert)
    # schedule.every().day.at("00:00").do(send_prominent_alert)
    while True:
        schedule.run_pending()
        time.sleep(1)
